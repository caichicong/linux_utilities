#!/usr/bin/env python

import sys
import commands

def main(argv):
    filename = argv[1]
    
    if filename.endswith('.tar.gz'):
        (status, output) = commands.getstatusoutput('tar -xzvf ' + filename)
        print output

    elif filename.endswith('.tar.bz2'):
        (status, output) = commands.getstatusoutput('tar -xjvf ' + filename)
        print output

    elif filename.endswith('.gz'):
        (status, output) = commands.getstatusoutput('gzip -d ' + filename)
        print output

    elif filename.endswith('.zip'):
        (status, output) = commands.getstatusoutput('unzip ' + filename)
        print output

if __name__ == '__main__':
    main(sys.argv)
